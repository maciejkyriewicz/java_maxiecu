/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package task_java;

import java.util.List;
import java.util.Scanner;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Maciej Tkacz
 */
public class MANAGEMENT_LIST {
     
    private List<MANUFACTURER> list;
    private JTextField text,text_2;
    private JComboBox combo; 
    private int i=0;
    private Object[][] data;
    
    public MANAGEMENT_LIST(List<MANUFACTURER> list,JTextField text,JTextField text_2,JComboBox combo,Object[][] data)
    {
        this.list = list;
        this.text = text;
        this.text_2 = text_2;
        this.combo= combo;
        this.data = data;
    }
    void addToList()
    {
        list.add(new MANUFACTURER());
    }
    void removeAll(){
       list.removeAll(list);
       for (int i=0;i<5;i++){
           for (int j=0;j<3;j++){
              data[i][j] = "";
           }
       }
       set_i(); 
    }
    void setName(){
       list.get(i).setName(text.getText());
       data[i][0] = list.get(i).getName();   
    }
    void setID(){
        Scanner sc = new Scanner(text_2.getText());
        if (sc.hasNextInt()){
            list.get(i).setId(Integer.parseInt(text_2.getText()));
            data[i][1] = list.get(i).getId();
        }
        else {
            JOptionPane.showMessageDialog(null,"NALEŻY WPISAĆ W ID ZMIENNA INT :)MISTRZUNIU ");  
        }
    }
    void setEngine(){
            if (combo.getSelectedIndex()==0){
               list.get(i).setFindEngine(true);
               data[i][2] = list.get(i).getFindEngine();
            }
            else {
               list.get(i).setFindEngine(false);
               data[i][2] = list.get(i).getFindEngine(); 
            }
    }
   int get_i(){
       return i; 
    }
   void set_i(){
       i=0;
   }
   void addToi(){
       i++;
   }
}
