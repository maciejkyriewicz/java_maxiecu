/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package task_java;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import static javax.swing.JFrame.setDefaultLookAndFeelDecorated;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Maciej Tkacz
 */


public class Task_Java extends JFrame {

    
    public Task_Java()
    {
        setSize(800,800);
        setLayout(new BorderLayout());
        setLocationRelativeTo(null);
        setDefaultLookAndFeelDecorated(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
 
    public static void addComponents(Container pane) {
        
        String[] columnNames = {"","","",};
       Object[][] data = {
    {"","", ""},
    {"","",""},
    {"","",""},
    {"","",""},
    {"","",""}
};
       JTable table = new JTable(data, columnNames);
       GridBagLayout border = new GridBagLayout();
       pane.setLayout(border);
       GridBagConstraints c = new GridBagConstraints();
       
       Font font = new Font("serif",Font.BOLD,30);
       JLabel label = new JLabel("    CAR      ");
       label.setHorizontalAlignment(SwingConstants.CENTER);
       label.setFont(font);
        c.weightx = 50;
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 1;
        pane.add(label, c);
       
       JLabel label_2 = new JLabel("   ID   ");
       label_2.setHorizontalAlignment(SwingConstants.CENTER);
       label_2.setFont(font);
        c.weightx = 0.5;
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 1;
        pane.add(label_2, c);
       
       JLabel label_3 = new JLabel(" ENGINE ");
       label_3.setHorizontalAlignment(SwingConstants.CENTER);
       label_3.setFont(font);
        c.weightx = 0.5;
        c.gridwidth = 1;
        c.gridx = 2;
        c.gridy = 1;
        pane.add(label_3, c);
       
       c.fill = GridBagConstraints.HORIZONTAL;
        c.ipady = 10;      //make this component tall
        c.weightx = 0.0;
        c.gridwidth = 3;
        c.gridx = 0;
        c.gridy = 0;
        pane.add(table, c);
       
        JButton button = new JButton("ADD");
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 3;
        pane.add(button, c);

        JPanel pane_2 = new JPanel();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridwidth = 2;
        c.gridx = 1;
        c.gridy = 3;
        pane.add(pane_2, c);
        
         JButton button_3 = new JButton("REMOVE ALL");
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridwidth = 2;
        c.gridx = 2;
        c.gridy = 3;
        pane.add(button_3, c);
        
           JTextField textfield = new JTextField("");
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = 2;
        pane.add(textfield, c);
        
           JTextField textfield_2 = new JTextField();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridwidth = 1;
        c.gridx = 1;
        c.gridy = 2;
        pane.add(textfield_2, c);
        
        JComboBox combo=new JComboBox();
        combo.addItem(true);
        combo.addItem(false);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridwidth = 1;
        c.gridx = 2;
        c.gridy = 2;
        pane.add(combo, c);
        
      List<MANUFACTURER>list =  new ArrayList();
      MANAGEMENT_LIST management_list = new MANAGEMENT_LIST(list,textfield,textfield_2,combo,data);
      //DefaultTableModel tablemodel = (DefaultTableModel) table.getModel();
       
       button.addActionListener(new ActionListener() {

        public void actionPerformed(ActionEvent e) {
           
           if (management_list.get_i()<5) {
                management_list.addToList();
                management_list.setName();
                management_list.setID();
                management_list.setEngine(); 
                management_list.addToi();
                //tableModel.fireTableDataChanged();
           }
        }
        });
        button_3.addActionListener(new ActionListener() {

        public void actionPerformed(ActionEvent e) {
           
           management_list.removeAll();
        }
        });
       
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        Task_Java mainWindow = new Task_Java();
        mainWindow.addComponents(mainWindow.getContentPane());
        mainWindow.pack();
        mainWindow.setVisible(true);
        
        
    }
    
}
